<?php

namespace Social;

use Zend\ServiceManager\ServiceManager;
use Social\Api\Factory;

class Module
{
	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

	public function getServiceConfig()
	{
		return array(
			'factories' => array(

                'ApiFactory' => function (ServiceManager $sm) {
                    $config = $sm->get('Config');
					return new Factory($config['social']);
				},
			)
		);
	}
}
