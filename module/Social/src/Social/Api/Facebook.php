<?php

namespace Social\Api;

use Zend\Http\Request;
use OAuth2\Client;
use Zend\Session\Container as SessionContainer;

class Facebook
{

    const SESSION_CONTAINER = 'facebook';
    const PROFILE_CONTAINER = 'profile';

    protected $transport;
    protected $config;

    protected $sessionContainer;
    protected $storage;

    public function __construct(Client $transport, $config)
    {
        $this->transport = $transport;
        $this->config = $config;

        $this->sessionContainer = new SessionContainer(self::SESSION_CONTAINER);
    }

    /**
     * Метод авторизации со сторонними сервисами через OAuth2
     *
     * @param Request $request
     *
     * @return array
     */
    public function authenticate(Request $request)
    {
        $code = $request->getQuery('code', null);
        $redirect_uri = 'http://' . $request->getUri()->getHost() . $this->config['redirect_url'];

        if ($code === null) {
            return array(
                'status' => 'code',
                'message' => $this->transport->getAuthenticationUrl($this->config['auth_url'], $redirect_uri)
            );
        } else {
            $params = array(
                'code' => $code,
                'redirect_uri' => $redirect_uri,
                'format' => 'json'
            );

            $response = $this->transport->getAccessToken($this->config['token_url'], 'authorization_code', $params);

            if (is_string($response['result'])) {
                parse_str($response['result'], $access);
            } else {
                $access = $response['result'];
            }

            if (!isset($access['access_token'])) {
                return array(
                    'status' => 'error',
                    'message' => 'Не получен access_token'
                );
            }

            $this->sessionContainer->offsetSet('access_token', $access['access_token']);
            $this->transport->setAccessToken($access['access_token']);

            return array(
                'status' => 'token',
                'message' => 'Токен получен'
            );
        }
    }

    public function setToken($token = false)
    {
        if (!$token) {
            $token = $this->sessionContainer->offsetGet('access_token');

            if (!$token) {
                throw new \Exception('Нет токена');
            }
        }

        $this->transport->setAccessToken($token);
        return $token;
    }

    public function hasToken()
    {
        return $this->sessionContainer->offsetExists('access_token');
    }

    public function clearToken()
    {
        $this->sessionContainer->offsetUnset('access_token');
    }

    protected function getSig($request_params)
    {
        ksort($request_params);
        $params = '';
        foreach ($request_params as $key => $value) {
            $params .= "$key=$value";
        }
        return md5($params . $this->transport->getClientSecret());
    }

    /**
     * Получить инфо по юзеру
     */
    public function getUser()
    {
        $response = $this->apiCall('me');
        $profile = $this->mapProfile($response);
        return $profile;
    }

    /**
     * Вызов метода API
     */
    protected function apiCall($method, $params= array())
    {
        $this->setToken();
        $response = $this->transport->fetch('https://graph.facebook.com/' . $method, $params);
        return $response['result'];
    }

    /**
     * Маппинг полей профиля facebook и таблицы user
     */
    public function mapProfile($profile) {
        return array(
            'id' => isset($profile['id']) ? $profile['id'] : '',
            'firstname' => isset($profile['first_name']) ? $profile['first_name'] : '',
            'lastname' => isset($profile['last_name']) ? $profile['last_name'] : '',
            'date_birth' => isset($profile['birthday']) ? $profile['birthday'] : '',
        );
    }
}
