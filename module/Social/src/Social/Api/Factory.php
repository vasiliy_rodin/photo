<?php

namespace Social\Api;

use OAuth2\Client;

class Factory {

    protected $config = null;
    protected $instances = array();

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getApi($name)
    {
        if (!isset($this->instances[$name])) {

            $apiConfig = $this->config[$name];
            $transport = new Client($apiConfig['client_id'], $apiConfig['client_secret']);

            $class = $apiConfig['class'];
            $this->instances[$name] = new $class($transport, $apiConfig['urls']);
        }

        return $this->instances[$name];
    }
}