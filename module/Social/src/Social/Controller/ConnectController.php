<?php

namespace Social\Controller;

use Application\Service\UserService;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;

class ConnectController extends AbstractActionController
{
    /**
     * Экшн, получающий доступ к сторонним сервисам
     *
     * @return array|\Zend\Http\Response
     */
    public function authAction()
    {
        /** @var \Social\Api\Factory $factory */
        $factory = $this->getServiceLocator()->get('ApiFactory');
        $apiName = $this->params()->fromRoute('type');

        /** @var Request $request */
        $request = $this->getRequest();

        // Если пользователь отклонил доступ к его аккаунту
        $error = $this->params()->fromQuery('error', null);

        if ($error !== null) {
            return array(
                'error_message' => $this->params()->fromQuery('error_reason', null)
            );
        }

        /** @var \Social\Api\Facebook $api */
        $api = $factory->getApi($apiName);
        $response = $api->authenticate($request);

        // Если ошибка - то кидаем на нужный урл
        if ($response['status'] == 'code') {
            return $this->redirect()->toUrl($response['message']);
        }

        // Достаем юзера из сессии
        $socialProfile = $api->getUser();

        // Смотрим, не был ли уже зарегистрирован соответствующий пользователь
        $user = $this->getUserService()->getUserByFacebookId($socialProfile['id']);

        if ($user && isset($user['email'])) {
            // Авторизация без пароля
            $this->getUserService()->getSecurityService()->auth($user['email'], false);
            return $this->redirect()->toRoute('list');
        }

        return $this->redirect()->toRoute('user.registration');

    }

    /**
     * @return UserService
     */
    private function getUserService()
    {
        return $this->getServiceLocator()->get('UserService');
    }

}