<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Social\Controller\Connect' => 'Social\Controller\ConnectController',
        ),
    ),

    'router' => array(
        'routes' => array(
            'social.auth' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/social/auth/:type/',
                    'defaults' => array(
                        'controller' => 'Social\Controller\Connect',
                        'action' => 'auth',
                    )
                ),
            )
        )
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view/',
        )
    )
);

