<?php

namespace Application\Security;

use Application\Exception\UserNotActivatedException;
use Application\Exception\UserNotFoundException;
use Application\Exception\WrongPasswordException;
use Application\Model\UserTable;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class Adapter implements AdapterInterface
{

    protected $mail = null;
    protected $password = null;
    protected $identity = null;

    protected $userTable;

    public function __construct(UserTable $userTable)
    {
        $this->userTable = $userTable;
    }

    public function setCredentials( $identity, $mail, $password )
    {
        $this->identity = $identity;
        $this->mail = $mail;
        $this->password = false === $password ? false : md5($password);
    }

    protected function createIdentity($userId, $email)
    {
        return new Identity((int) $userId, $email);
    }

    /**
     * Аутентификация
     */
    public function authenticate()
    {
        try {
            $user = $this->userTable->findOneByEmail($this->mail);

            // Пользователь не найден?
            if (!$user) {
                throw new UserNotFoundException();
            }

            // Проверяем пароль
            if (!(false === $this->password || $user['password'] === $this->password)) {
                throw new WrongPasswordException();
            }

            // Пользователь активирован?
            if ($user['activated'] == 0) {
                throw new UserNotActivatedException();
            }

            $result = new Result(
                Result::SUCCESS,
                $this->createIdentity($user['id'], $user['email'])
            );

        } catch (WrongPasswordException $e) {
            $result = new Result(Result::FAILURE_CREDENTIAL_INVALID, $this->identity, array($e->getMessage()));
        } catch (UserNotFoundException $e) {
            $result = new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $this->identity, array($e->getMessage()));
        } catch (UserNotActivatedException $e) {
            $result = new Result(Result::FAILURE_IDENTITY_NOT_FOUND, $this->identity, array($e->getMessage()));
        }

        // Сбрасываем параметры
        $this->identity = $this->mail = $this->password = null;
        return $result;
    }
}