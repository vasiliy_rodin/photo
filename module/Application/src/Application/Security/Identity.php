<?php

namespace Application\Security;

class Identity
{
	protected $id;
    protected $email;

    public function __construct($id = null, $email = null)
	{
		$this->id = (int) $id;
		$this->email = mb_strtolower($email);
	}


	public function getId()
	{
		return $this->id;
	}

	public function getMail()
	{
		return $this->email;
	}

}
