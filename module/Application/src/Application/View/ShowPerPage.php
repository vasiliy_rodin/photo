<?php

namespace Application\View;

use Application\Service\PhotoService;
use Zend\Http\PhpEnvironment\Request;
use Zend\View\Helper\AbstractHelper;

class ShowPerPage extends AbstractHelper
{
    private static $perPageValues = array(1, 5, 10, 20, 50);

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function __invoke($url = '')
    {
        return $this->getView()->render('helper/show_per_page', array(
            'values' => self::$perPageValues,
            'current' => $this->request->getQuery('per_page') ? : PhotoService::DEFAULT_PAGE_PHOTO_COUNT,
        ));
    }

}
