<?

namespace Application\Service;

use Application\Model\PhotoTable;
use Zend\Db\Adapter\Adapter;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class PhotoService
{

    const DEFAULT_PAGE_PHOTO_COUNT = 5;
    const MAX_PAGE_PHOTO_COUNT = 100;

    private $uploadService;
    private $photoTable;
    private $securityService;

    /**
     * Конструктор
     */
    public function __construct(SecurityService $securityService,
                                UploadService $uploadService,
                                PhotoTable $photoTable)
    {
        $this->securityService = $securityService;
        $this->uploadService = $uploadService;
        $this->photoTable = $photoTable;
    }

    /**
     * Добавить фотографию
     */
    public function addPhoto($currentFileName, $data)
    {
        $newFileName = $this->uploadPhoto($currentFileName);

        // Добавляем к данным id текущего юзера
        $data += array(
            'user_id' => $this->securityService->getIdentity()->getId(),
            'filename' => $newFileName,
        );

        // Добавляем данные в таблице
        return $this->photoTable->insert($data);
    }

    /**
     * Аплоад фото через UploadService
     */
    private function uploadPhoto($currentFileName)
    {
        $uploadService = $this->uploadService;
        $uploadService->setCurrentFile($currentFileName);

        // TODO: Пока генерируем имя файла исходя из времени и id юзера
        $base = md5($this->securityService->getIdentity()->getId() . '|' . microtime());

        $newFileName = $uploadService->generateNewFileName($base);
        $uploadService->uploadFile($newFileName);
        $uploadService->makeThumbnail($newFileName);

        return $newFileName;
    }

    /**
     * Получить пагинатор
     *
     * @param int $page    Номер страницы
     * @param int $perPage Количество элементов на странице
     *
     * @return Paginator
     */
    public function getPaginator($page, $perPage)
    {
        /** @var Adapter $adapter */
        $adapter = $this->photoTable->getAdapter();
        $select = $this->photoTable->getSelect();

        $dbSelect  = new DbSelect($select, $adapter);
        $paginator = new Paginator($dbSelect);

        if ($perPage <= 0) {
            $perPage = self::DEFAULT_PAGE_PHOTO_COUNT;
        }

        if ($perPage > self::MAX_PAGE_PHOTO_COUNT) {
            $perPage = self::MAX_PAGE_PHOTO_COUNT;
        }

        $paginator->setItemCountPerPage($perPage);

        if ($page) {
            $paginator->setCurrentPageNumber($page);
        }

        return $paginator;
    }




}