<?

namespace Application\Service;

use Application\Security\Identity;
use Zend\Authentication\AuthenticationService;
use Application\Security\Adapter as SecurityAdapter;

/**
 * @property SecurityAdapter adapter
 * @method Identity getIdentity
 */
class SecurityService extends AuthenticationService
{

    /**
     * Авторизация
     * Принудительная авторизация - $password === false
     *
     * @param $login
     * @param $password
     *
     * @return \Zend\Authentication\Result
     */
    public function auth($login, $password)
    {
        $this->adapter->setCredentials($this->getIdentity(), $login, $password);
        $result = parent::authenticate();
        return $result;
    }

}