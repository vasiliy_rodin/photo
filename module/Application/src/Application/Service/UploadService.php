<?php

namespace Application\Service;

use Application\Exception\UploadServiceException;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Zend\File\Transfer\Adapter\Http as FileAdapter;
use Zend\Validator\File\MimeType;
use Zend\Validator\File\Size;
use Zend\Validator\File\Extension;
use Zend\Validator\File\Count;
use Imagine\Image\Point;

class UploadService
{
    const EXTENSIONS_ALIAS      = 'extensions';
    const THUMB_ALIAS           = 'thumb';
    const MAX_SIZE_ALIAS        = 'max_size';
    const DESTINATION_DIR_ALIAS = 'destination_dir';

    const THUMB_PATH_ALIAS      = 'path';
    const THUMB_WIDTH_ALIAS     = 'width';
    const THUMB_HEIGHT_ALIAS    = 'height';
    const THUMB_MODE_ALIAS      = 'mode';

    /**
     * @var array
     */
    private $config;

    /**
     * @var string
     */
    private $currentFile;

    /**
     * @var Imagine
     */
    private $imagine;

    /**
     * Конструктор
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->imagine = new Imagine();
    }


    public function setCurrentFile($name)
    {
        $this->getFileByName($name);
        $this->currentFile = $name;
    }

    /**
     * Сгенерировать новое имя файла
     */
    public function generateNewFileName($newName)
    {
        $file = $this->getCurrentFile();

        if (!isset($file['type'])) {
            throw new UploadServiceException('Не задан тип файла');
        }

        return $newName . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
    }

    /**
     * Загрузить и переименовать файл
     */
    public function uploadFile($newFilename)
    {
        $config = $this->getConfig();

        // Путь к загружаемому файлу
        $newFilePath = getcwd() . '/' . $config[self::DESTINATION_DIR_ALIAS] . '/' . $newFilename;

        $adapter = new FileAdapter();

        $adapter->setValidators(array(
            // Расширение файла
            new Extension(array('extension' => $this->getExtensions($config))),

            // Mime type
            new MimeType($this->getMimeTypes()),

            // Количество файлов
            new Count(array('max' => 1)),

            // Размер файла
            new Size(array('min' => 1, 'max' => $config[self::MAX_SIZE_ALIAS]))
        ));

        // Переименовываем файл
        $adapter->addFilter('Rename', array(
            'target'    => $newFilePath,
            'overwrite' => true
        ), $this->currentFile);

        // Файл загрузился?
        if (!$adapter->isUploaded($this->currentFile)) {
            throw new UploadServiceException('Файл не загрузился');
        }

        // Сработал один из валидаторов?
        if (!$adapter->isValid($this->currentFile)) {
            $errors = $adapter->getMessages();
            $error  = count($errors) ? array_shift($errors) : 'Ошибка загрузки файла';
            throw new UploadServiceException($error);
        }

        // Окончательная обработка файла
        if (!$adapter->receive($this->currentFile)) {
            $errors = $adapter->getMessages();
            throw new UploadServiceException(count($errors) ? array_shift($errors) : 'Ошибка загрузки файла');
        }

    }

    /**
     * Создать миниатюру
     */
    public function makeThumbnail($newFilename)
    {
        $thumbConfig = $this->getThumbConfig();
        $config      = $this->getConfig();

        $oldPath = $config[self::DESTINATION_DIR_ALIAS] . '/' . $newFilename;
        $savePath = $config[self::DESTINATION_DIR_ALIAS] . '/m_' . $newFilename;

        $width  = (int) $thumbConfig[self::THUMB_WIDTH_ALIAS];
        $height = (int) $thumbConfig[self::THUMB_HEIGHT_ALIAS];

        $this->makeImagineThumbnail($oldPath, $savePath, $width, $height);

    }

    /**
     * Проверить существует ли файл
     *
     * @return bool
     */
    public function hasFile()
    {
        try {
            $this->getCurrentFile();
            return true;
        } catch (UploadServiceException $e) {
            return false;
        }
    }

    /**
     * Получить конфигурацию
     */
    private function getConfig()
    {
        return $this->config;
    }

    /**
     * Получить конфигурации для миниатюры
     */
    private function getThumbConfig()
    {
        $config = $this->getConfig();

        if (!isset($config[self::THUMB_ALIAS])) {
            throw new UploadServiceException('Конфигурация копии не найдена');
        }

        $thumbConfig = $config[self::THUMB_ALIAS];

        return $thumbConfig;
    }

    /**
     * Получить файл по имени
     */
    private function getFileByName($name)
    {
        $adapter = new FileAdapter();
        $files = $adapter->getFileInfo();

        $file = isset($files[$name]) ? $files[$name] : null;

        if (!$file) {
            throw new UploadServiceException('Файл отсутствует');
        }

        return $file;
    }

    /**
     * Получить информацию о текущем файле
     */
    private function getCurrentFile()
    {
        if (!$this->currentFile) {
            throw new UploadServiceException('Файл не выбран');
        }

        return $this->getFileByName($this->currentFile);
    }

    /**
     * Получить расширение файла по mimeType
     */
    private function guessExtensionByMimeType($mimeType)
    {
        $config = $this->getConfig();
        $extension = isset($config[self::EXTENSIONS_ALIAS][$mimeType]) ? $config[self::EXTENSIONS_ALIAS][$mimeType] : null;

        return $extension ? (is_array($extension) ? array_shift($extension) : $extension) : null;
    }

    /**
     * Получить список доступных расширений
     */
    private function getExtensions()
    {
        $config = $this->getConfig();
        $extensions = array();

        foreach ($config[self::EXTENSIONS_ALIAS] as $mimeTypeExtensions) {
            if (is_array($mimeTypeExtensions)) {
                foreach ($mimeTypeExtensions as $extension) {
                    $extensions[] = $extension;
                }
            } elseif (is_string($mimeTypeExtensions)) {
                $extensions[] = $mimeTypeExtensions;
            }
        }

        return array_unique($extensions);
    }

    /**
     * Получить список доступных типов файлов
     */
    private function getMimeTypes()
    {
        $config = $this->getConfig();
        return is_array($config[self::EXTENSIONS_ALIAS]) ? array_keys($config[self::EXTENSIONS_ALIAS]) : array();
    }

    /**
     * Сделать миниатюру с помощью Imagine
     */
    private function makeImagineThumbnail($oldPath, $savePath, $width, $height)
    {
        $size = new Box($width, $height);

        $image = $this->imagine->open($oldPath);

        $ratios = array(
            $width / imagesx($image->getGdResource()),
            $height / imagesy($image->getGdResource())
        );

        $ratio = max($ratios);

        $thumbnail = $image;
        $thumbnailSize = $thumbnail->getSize()->scale($ratio);
        $thumbnail->resize($thumbnailSize);
        $thumbnail->crop(new Point(0, 0), $size);
        $thumbnail->save($savePath, array('quality' => 90));
    }

}