<?

namespace Application\Service;

use Application\Model\UserTable;

class UserService
{

    const SALT = 'vs2CSsZPP!#dz';

    private $userTable;
    private $securityService;

    /**
     * Конструктор
     */
    public function __construct(SecurityService $securityService, UserTable $userTable)
    {
        $this->securityService = $securityService;
        $this->userTable = $userTable;
    }

    /**
     * @return \Application\Service\SecurityService
     */
    public function getSecurityService()
    {
        return $this->securityService;
    }

    /**
     * Зарегистрировать пользователя
     */
    public function register($data)
    {
        return $this->userTable->insert($data);
    }

    /**
     * Найти пользователя по FacebookId
     */
    public function getUserByFacebookId($facebookId)
    {
        return $this->userTable->findOneBy(array('facebook_id' => $facebookId));
    }

    /**
     * Хэш пользователя для активации
     */
    public function getHash($email)
    {
        return md5(self::SALT . '|' . $email);
    }

    /**
     * Активируем по e-mail
     */
    public function activateByEmail($email)
    {
        $user = $this->getUserByEmail($email);

        if (!$user || $user['activated'] == 1) {
            return false;
        }

        // TODO: этот пароль нужно высылать пользователю
        $password = rand(1, 1000000);

        $this->userTable->update($user['id'], array(
            'password' => md5($password),
            'activated' => 1
        ));

        return true;
    }

    public function getUserByEmail($email)
    {
        return $this->userTable->findOneBy(array('email' => $email));
    }


}