<?

namespace Application\Service;

use Application\Model\PhotoTable;
use Application\Model\VoteTable;

class VoteService
{
    protected $config;

    public function __construct(SecurityService $securityService, VoteTable $voteTable, PhotoTable $photoTable) {
        $this->securityService = $securityService;
        $this->voteTable = $voteTable;
        $this->photoTable = $photoTable;
    }

    /**
     * Добавить голос
     */
    public function addVote($photoId)
    {
        // Берем текущего юзера
        $userId = $this->securityService->getIdentity()->getId();

        // Добавляем голос
        $success = $this->voteTable->addVote($userId, $photoId);

        // Если голос не добавился - значит уже голосовали
        if ($success === false) {
            return false;
        }

        // Пересчитываем голоса в таблице photo
        $newCount = $this->voteTable->countVotes($photoId);
        $this->photoTable->updateVotes($photoId, $newCount);

        return $newCount;
    }

}