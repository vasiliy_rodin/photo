<?

namespace Application\Service;

use Zend\Mail\Message as MailMessage;
use Zend\Mail\Transport\Sendmail;
use Zend\Mime\Part;

class MailService
{
    protected $config;

    public function __construct($config) {
        $this->config = $config;
    }

    public function sendActivation($email, $hash)
    {
        $link = $this->config['domain'] . '/user/activation/' . $email . '/' . $hash . '/';

        // TODO: перенести в отдельный файл
        $body = <<<HTML
<p>Вам необходимо активировать свой аккаунт по ссылке:</p>
<a href="$link">$link</a>
HTML;

        $bodyPart = new \Zend\Mime\Message();

        $bodyMessage = new Part($body);
        $bodyMessage->type = 'text/html';

        $bodyPart->setParts(array($bodyMessage));

        $mail = new MailMessage();
        $mail->setBody($bodyPart);
        $mail->setFrom($this->config['sender_mail'], $this->config['sender_name']);
        $mail->addTo($email);
        $mail->setSubject('Регистрация на сайте');

        $transport = new Sendmail();
        $transport->send($mail);

    }

}