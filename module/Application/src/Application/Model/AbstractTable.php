<?php

namespace Application\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

/**
 * Common Table class - extend it and use for your tables
 */
abstract class AbstractTable
{
    protected $config;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function executeSelect($select)
    {
        $sql = $this->tableGateway->getSql();
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return array_values(iterator_to_array($result));
    }

    public function getList($filter = array(), $limit = null, $offset = null)
    {
        $select = $this->tableGateway->getSql()->select()->where($filter);

        if ($limit) {
            $select = $select->limit((int)$limit);
        }

        if ($offset) {
            $select = $select->offset((int)$offset);
        }

        return $this->executeSelect($select);
    }

    public function findOneBy($where = array())
    {
        $select = $this->tableGateway->getSql()->select()->where($where);
        $result = $this->executeSelect($select);
        return (!empty($result)) ? array_shift($result) : null;
    }

    public function findBy($where = array())
    {
        $select = $this->tableGateway->getSql()->select()->where($where);
        return $this->executeSelect($select);
    }

    public function delete($where = array())
    {
        return $this->tableGateway->delete($where);
    }

    public function count($where = array())
    {
        $select = $this->tableGateway->getSql()->select()
            ->columns(array('count' => new Expression('COUNT(*)')))->where($where);
        $result = $this->executeSelect($select);
        return isset($result[0]['count']) ? $result[0]['count'] : 0;
    }

    /**
     * Update by id
     *
     * @param int $id ID
     * @param array $data Data
     *
     * @return void
     */
    public function update($id, $data)
    {
        $this->tableGateway->update($data, array('id' => $id));
    }

    /**
     * Insert
     *
     * @param array $data Data
     *
     * @return int
     */
    public function insert($data)
    {
        $this->tableGateway->insert($data);
        return $this->tableGateway->getLastInsertValue();
    }

    /**
     * Get adapter
     *
     * @return \Zend\Db\Adapter\AdapterInterface
     */
    public function getAdapter()
    {
        return $this->tableGateway->getAdapter();
    }

    /**
     * Get table gateway
     *
     * @return \Zend\Db\TableGateway\TableGateway
     */
    public function getTableGateway()
    {
        return $this->tableGateway;
    }

    /**
     * Get table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->getTableGateway()->getTable();
    }

}