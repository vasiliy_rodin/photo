<?php
namespace Application\Model;

class PhotoTable extends AbstractTable
{

    /**
     * Select запрос для построения списка
     */
    public function getSelect()
    {
        $select = $this->getTableGateway()->getSql()->select();

        // TODO: этот join будет не очень производительным на больших объемах
        $select->join(
            'user',
            'user.id = '.$this->getTableName().'.user_id',
            array('firstname', 'lastname')
        );

        $select->order(array('created' => 'DESC'));

        return $select;
    }

    /**
     * Обновить количество голосов
     */
    public function updateVotes($photoId, $count)
    {
        $this->update($photoId, array('votes' => $count));
    }
}