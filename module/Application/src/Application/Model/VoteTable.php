<?php
namespace Application\Model;

class VoteTable extends AbstractTable
{

    /**
     * Добавить голос
     */
    public function addVote($userId, $photoId)
    {
        $data = array('user_id' => $userId, 'photo_id' => $photoId);

        if ($this->findOneBy($data)) {
            return false;
        }

        return $this->insert($data);
    }

    /**
     * Посчитать голоса для конкретной фотографии
     */
    public function countVotes($photoId)
    {
        return $this->count(array('photo_id' => $photoId));
    }


}