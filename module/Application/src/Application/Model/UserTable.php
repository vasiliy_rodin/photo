<?php
namespace Application\Model;


class UserTable extends AbstractTable
{

    public function findOneByEmail($email)
    {
        return $this->findOneBy(array('email' => $email));
    }

}