<?php

namespace Application\Exception;

use Exception;

class UserNotActivatedException extends Exception
{
    const MESSAGE = 'Пользователь не активирован';

	public function __construct()
	{
		parent::__construct(self::MESSAGE);
	}
}
