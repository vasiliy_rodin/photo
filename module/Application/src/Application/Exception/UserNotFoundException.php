<?php

namespace Application\Exception;

use Exception;

class UserNotFoundException extends Exception
{
    const MESSAGE = 'Пользователь не зарегистрирован';

	public function __construct()
	{
		parent::__construct(self::MESSAGE);
	}
}
