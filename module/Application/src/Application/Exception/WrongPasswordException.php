<?php

namespace Application\Exception;

use Exception;

class WrongPasswordException extends Exception
{
    const MESSAGE = 'Неправильный пароль';

	public function __construct()
	{
		parent::__construct(self::MESSAGE);
	}
}
