<?php

namespace Application\Form;

use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Exception;
use Zend\Form\Form;

use Zend\Form\FormInterface;

class Login extends Form
{
    public function __construct()
    {
        parent::__construct('login-form');

        $this->setAttribute('method', 'post');

        $this->add(new Text('email', array('label' => 'E-mail')));
        $this->add(new Password('password', array('label' => 'Пароль')));

        $submit = new Submit('submit');
        $submit->setValue('Войти');
        $this->add($submit);
    }

    public function getData($flag = FormInterface::VALUES_NORMALIZED)
    {
        $data = parent::getData($flag);
        unset($data['submit']);

        return $data;
    }


}