<?php

namespace Application\Form;

use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Exception;
use Zend\Form\Form;

use Application\Form\Filter\User as UserFilter;
use Zend\Form\FormInterface;

class User extends Form
{
    public function __construct()
    {
        parent::__construct('registration-form');

        $this->setAttribute('method', 'post');

        $this->add(new Text('email', array('label' => 'E-mail')));

        $this->add(new Text('firstname', array('label' => 'Имя')));
        $this->add(new Text('lastname', array('label' => 'Фамилия')));

        $this->add(new Text('phone', array('label' => 'Телефон')));
        $this->add(new Text('date_birth', array('label' => 'Дата рождения')));

        $submit = new Submit('submit');
        $submit->setValue('Зарегистрироваться');
        $this->add($submit);

        $this->setInputFilter(new UserFilter());
    }

    public function getData($flag = FormInterface::VALUES_NORMALIZED)
    {
        $data = parent::getData($flag);
        unset($data['submit']);

        return $data;
    }


}