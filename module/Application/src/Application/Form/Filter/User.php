<?php

namespace Application\Form\Filter;

use Zend\Filter\StringTrim;
use Zend\InputFilter\InputFilter;
use Zend\Validator\Callback;
use Zend\Validator\EmailAddress;

class User extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'firstname',
            'required' => true,
            'filters' => array(
                new StringTrim(),
            ),
        ));

        $this->add(array(
            'name' => 'lastname',
            'required' => true,
            'filters' => array(
                new StringTrim(),
            ),
        ));

        $this->add(array(
            'name' => 'date_birth',
            'required' => true,
            'filters' => array(
                new StringTrim(),
            ),
            'validators' => array(
                array(
                    'name'                   => 'Callback',
                    'break_chain_on_failure' => true,
                    'options'                => array(
                        'messages' => array(
                            Callback::INVALID_VALUE => 'Неправильный формат даты рождения',
                        ),
                        'callback' => function($value) {
                            try {
                                new \DateTime($value);
                                return true;
                            } catch (\Exception $e) {
                                return false;
                            }
                        }
                        )
                    ),
                    new Callback(array(
                        'messages' => array(
                            Callback::INVALID_VALUE => 'Дата рождения не должна быть в будущем',
                        ),
                        'callback' => function($value) {
                            $dateBirth = new \DateTime($value);
                            return $dateBirth->format('Y-m-d') <= date('Y-m-d');
                        }
                    )),
                    new Callback(array(
                        'messages' => array(
                            Callback::INVALID_VALUE => 'Дата рождения не должна превышать 120 лет',
                        ),
                        'callback' => function($value) {
                            $dateBirth = new \DateTime($value);
                            $dateMin = new \DateTime();
                            $dateMin->modify('-120 years');

                            return $dateBirth->format('Y-m-d') >= $dateMin->format('Y-m-d');
                        },
                    )
                ),
            )
        ));

        $this->add(array(
            'name' => 'email',
            'required' => true,
            'validators' => array(
                new EmailAddress(),
            ),
            'filters' => array(
                new StringTrim(),
            ),
        ));

        $this->add(array(
            'name' => 'phone',
            'required' => false,
            'filters' => array(
                new StringTrim(),
            ),
        ));

    }
}