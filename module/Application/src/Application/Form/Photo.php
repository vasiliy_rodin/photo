<?php

namespace Application\Form;

use Zend\Form\Element\File;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Exception;
use Zend\Form\Form;

use Zend\Form\FormInterface;
use Application\Form\Filter\Photo as PhotoFilter;

class Photo extends Form
{
    public function __construct()
    {
        parent::__construct('photo-form');

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(new Text('title', array('label' => 'Заголовок')));
        $this->add(new File('photo', array('label' => 'Фото')));

        $submit = new Submit('submit');
        $submit->setValue('Загрузить');
        $this->add($submit);

        $this->setInputFilter(new PhotoFilter());
    }

    public function getData($flag = FormInterface::VALUES_NORMALIZED)
    {
        $data = parent::getData($flag);
        unset($data['submit']);

        return $data;
    }


}