<?php

namespace Application\Controller;

use Application\Service\MailService;
use Application\Service\UserService;
use Social\Api\Facebook;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\PhpEnvironment\Request;

use Application\Form\User as UserForm;
use Application\Form\Login as LoginForm;

class UserController extends AbstractActionController
{

    const MESSAGE_REGISTRATION_SUCCESS = 'Вы успешно зарегистрировались. Проверьте почту';

    /**
     * Логин
     */
    public function loginAction()
    {
        // Если авторизованы - переходим на список
        if ($this->identity()) {
            return $this->redirect()->toRoute('list');
        }

        /** @var Request $request */
        $request = $this->getRequest();

        // Инициализируем форму
        $form = new LoginForm();
        $form->prepare();

        $view = new ViewModel(array(
            'form' => $form
        ));

        // Если запрос не POST - возвращаем форму
        if (!$request->isPost()) {
            return $view;
        }

        $post = $request->getPost();
        $form->setData($post);

        // Если данные невалидны - возвращаем форму с ошибками
        if (!$form->isValid()) {
            return $view;
        }

        $securityService = $this->getUserService()->getSecurityService();

        $data = $form->getData();

        // Авторизуем
        $result = $securityService->auth($data['email'], $data['password']);

        // Если не удалось авторизоваться - отдаем ошибку
        if (!$result->isValid()) {
            $form->setMessages($form->getMessages() + array('password' => $result->getMessages()));
            return $view;
        }

        return $this->redirect()->toRoute('list');
    }

    /**
     * Регистрация
     */
    public function registrationAction()
    {
        // Если авторизованы - переходим на список
        if ($this->identity()) {
            return $this->redirect()->toRoute('list');
        }

        /** @var Request $request */
        $request = $this->getRequest();

        // Инициализируем форму
        $form = new UserForm();

        $view = new ViewModel(array(
            'form' => $form
        ));

        // Если запрос не POST - возвращаем форму
        if (!$request->isPost()) {

            // Проверяем - есть ли в наличии данные из фейсбука
            if ($this->getFacebookService()->hasToken() && $profile = $this->getFacebookService()->getUser()) {
                $form->setData($profile);
            }

            return $view;
        }

        $post = $request->getPost();
        $form->setData($post);

        // Если данные невалидны - возвращаем форму с ошибками
        if (!$form->isValid()) {
            return $view;
        }

        // Достаем данные
        $data = $form->getData();

        // Проверяем, зарегистрирован ли уже такой e-mail
        // TODO: можно перенести эту проверку в форму
        if ($this->getUserService()->getUserByEmail($data['email'])) {
            $form->setMessages(array('email' => array('Пользователь с таким e-mail уже зарегистрирован')));
            return $view;
        }

        // Добавляем facebook_id
        if ($this->getFacebookService()->hasToken() && $profile = $this->getFacebookService()->getUser()) {
            $data['facebook_id'] = $profile['id'];

            // Стираем токен
            $this->getFacebookService()->clearToken();
        }

        // Регистрируем
        $this->getUserService()->register($data);

        // Сообщение об успешной регистрации
        $this->flashMessenger()->addSuccessMessage(self::MESSAGE_REGISTRATION_SUCCESS);

        // Отправляем письмо
        $this->getMailService()->sendActivation($data['email'], $this->getUserService()->getHash($data['email']));

        return $this->redirect()->toRoute('index');
    }

    /**
     * Активация
     */
    public function activationAction()
    {

        $email = $this->params()->fromRoute('email');
        $hash = $this->params()->fromRoute('hash');

        if( $hash !== $this->getUserService()->getHash($email) ) {
            return $this->redirect()->toUrl('/');
        }

        // Активируем по e-mail
        $this->getUserService()->activateByEmail($email);

        // Авторизуем
        $this->getUserService()->getSecurityService()->auth($email, false);

        // Редиректим
        return $this->redirect()->toRoute('list');
    }

    public function logoutAction()
    {
        $securityService = $this->getUserService()->getSecurityService();
        $securityService->clearIdentity();

        return $this->redirect()->toRoute('index');
    }

    /**
     * @return UserService
     */
    private function getUserService()
    {
        return $this->getServiceLocator()->get('UserService');
    }

    /**
     * @return MailService
     */
    private function getMailService()
    {
        return $this->getServiceLocator()->get('MailService');
    }

    /**
     * @return Facebook
     */
    private function getFacebookService()
    {
        return $this->getServiceLocator()->get('ApiFactory')->getApi('facebook');
    }

}
