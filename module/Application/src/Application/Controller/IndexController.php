<?php

namespace Application\Controller;

use Application\Exception\UploadServiceException;
use Application\Service\PhotoService;
use Application\Service\VoteService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Http\PhpEnvironment\Request;
use Application\Form\Photo as PhotoForm;

class IndexController extends AbstractActionController
{

    const MESSAGE_ADD_PHOTO_SUCCESS = 'Фото успешно добавлено';

    /**
     * Главная страница
     */
    public function indexAction()
    {
        return new ViewModel();
    }

    /**
     * Добавление фото
     */
    public function photoAddAction()
    {
        // Если не авторизованы - переходим на главную
        if (!$this->identity()) {
            return $this->redirect()->toRoute('index');
        }

        /** @var Request $request */
        $request = $this->getRequest();

        // Инициализируем форму
        $form = new PhotoForm();

        $view = new ViewModel(array(
            'form' => $form
        ));

        // Если запрос не POST - возвращаем форму
        if (!$request->isPost()) {
            return $view;
        }

        $post = $request->getPost();
        $form->setData($post);

        // Если данные невалидны - возвращаем форму с ошибками
        if (!$form->isValid()) {
            return $view;
        }

        try {

            // Добавляем фото в базу
            $this->getPhotoService()->addPhoto('photo', array(
                'title' => $form->get('title')->getValue()
            ));

        } catch (UploadServiceException $e) {
            $messages = array('photo' => array($e->getMessage()));
            $form->setMessages($messages);
            return $view;
        }



        // Сообщение об успешной загрузке фото
        $this->flashMessenger()->addSuccessMessage(self::MESSAGE_ADD_PHOTO_SUCCESS);

        return $this->redirect()->toRoute('list');
    }

    /**
     * Список фотографий
     */
    public function listAction()
    {
        // Если не авторизованы - переходим на главную
        if (!$this->identity()) {
            return $this->redirect()->toRoute('index');
        }

        $page = $this->params()->fromRoute('page');
        $perPage = $this->params()->fromQuery('per_page');

        return new ViewModel(array(
            'paginator' => $this->getPhotoService()->getPaginator($page, $perPage),
            'perPage' => $perPage
        ));
    }

    /**
     * Голосование (AJAX)
     */
    public function voteAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        // Не даем проходить без POST AJAX
        if (!$request->isXmlHttpRequest() || !$request->isPost()) {
            return $this->redirect()->toRoute('index');
        }

        // Не даем проходить без авторизации
        if (!$this->identity()) {
            return $this->redirect()->toRoute('index');
        }

        $photoId = $request->getPost('photo_id');

        // Добавляем голос
        $count = $this->getVoteService()->addVote($photoId);

        return new JsonModel(array(
            'success' => $count !== false,
            'count' => $count
        ));

    }

    /** @return PhotoService */
    private function getPhotoService()
    {
        return $this->getServiceLocator()->get('PhotoService');
    }

    /** @return VoteService */
    private function getVoteService()
    {
        return $this->getServiceLocator()->get('VoteService');
    }

}
