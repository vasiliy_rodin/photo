<?php

return array(
    'index' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/',
            'defaults' => array(
                'controller' => 'Application\Controller\Index',
                'action' => 'index',
            ),
        ),
    ),

    'user.login' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/login/',
            'defaults' => array(
                'controller' => 'Application\Controller\User',
                'action' => 'login'
            ),
        ),
    ),

    'user.logout' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/logout/',
            'defaults' => array(
                'controller' => 'Application\Controller\User',
                'action' => 'logout'
            ),
        ),
    ),

    'user.activation' => array(
        'type' => 'segment',
        'options' => array(
            'route' => '/user/activation/:email/:hash/',
            'defaults' => array(
                'controller' => 'Application\Controller\User',
                'action' => 'activation',
            ),
        ),
    ),

    'user.registration' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/registration/',
            'defaults' => array(
                'controller' => 'Application\Controller\User',
                'action' => 'registration',
            ),
        ),
    ),

    'list' => array(
        'type' => 'segment',
        'options' => array(
            'route' => '/list[/page/:page]/',
            'defaults' => array(
                'controller' => 'Application\Controller\Index',
                'action' => 'list',
            ),
        ),
    ),

    'photo.add' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/photo/add/',
            'defaults' => array(
                'controller' => 'Application\Controller\Index',
                'action' => 'photoAdd',
            ),
        ),
    ),

    'vote' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/vote/',
            'defaults' => array(
                'controller' => 'Application\Controller\Index',
                'action' => 'vote',
            ),
        ),
    ),

);