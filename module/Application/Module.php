<?php

namespace Application;

use Application\Model\PhotoTable;
use Application\Model\UserTable;
use Application\Model\VoteTable;
use Application\Service\MailService;
use Application\Service\PhotoService;
use Application\Service\SecurityService;
use Application\Service\UploadService;
use Application\Service\UserService;
use Application\Service\VoteService;
use Application\View\ShowPerPage;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGateway;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\AbstractPluginManager;
use Zend\ServiceManager\ServiceManager;

use Zend\Authentication\Storage\Session as SessionStorage;
use Application\Security\Adapter as SecurityAdapter;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Translator\TranslatorInterface;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function onBootstrap(MvcEvent $e)
    {
        // Init translator

        /** @var TranslatorInterface $translator */
        $translator = $e->getApplication()->getServiceManager()->get('MvcTranslator');
        AbstractValidator::setDefaultTranslator($translator, 'default');
    }


    public function getServiceConfig()
    {

        return array(

            'factories' => array(

                'SecurityService' => function (ServiceManager $sm) {

                    /** @var UserTable $userTable */
                    $userTable = $sm->get('UserTable');

                    $adapter = new SecurityAdapter($userTable);

                    return new SecurityService(
                        new SessionStorage(),
                        $adapter
                    );

                },

                'UserService' => function (ServiceManager $sm) {

                    /** @var UserTable $userTable */
                    $userTable = $sm->get('UserTable');

                    /** @var SecurityService $securityService */
                    $securityService = $sm->get('SecurityService');

                    $userService = new UserService($securityService, $userTable);

                    return $userService;
                },

                'MailService' => function (ServiceManager $sm) {

                    $config = $sm->get('Config');

                    $mailService = new MailService($config);

                    return $mailService;
                },

                'UploadService' => function (ServiceManager $sm) {

                    $config = $sm->get('Config');
                    $uploadConfig = isset($config['upload']) ? $config['upload'] : array();

                    $uploadService = new UploadService($uploadConfig);

                    return $uploadService;
                },

                'PhotoService' => function (ServiceManager $sm) {

                    /** @var PhotoTable $photoTable */
                    $photoTable = $sm->get('PhotoTable');

                    /** @var SecurityService $securityService */
                    $securityService = $sm->get('SecurityService');

                    /** @var UploadService $uploadService */
                    $uploadService = $sm->get('UploadService');

                    $photoService = new PhotoService($securityService, $uploadService, $photoTable);

                    return $photoService;
                },

                'VoteService' => function (ServiceManager $sm) {

                    /** @var VoteTable $voteTable */
                    $voteTable = $sm->get('VoteTable');

                    /** @var PhotoTable $photoTable */
                    $photoTable = $sm->get('PhotoTable');

                    /** @var SecurityService $securityService */
                    $securityService = $sm->get('SecurityService');

                    $voteService = new VoteService($securityService, $voteTable, $photoTable);

                    return $voteService;
                },

                'UserTable' => function (ServiceManager $sm) {

                    /** @var Adapter $db */
                    $db = $sm->get('Zend\Db\Adapter\Adapter');

                    return new UserTable(new TableGateway('user', $db));

                },

                'PhotoTable' => function (ServiceManager $sm) {

                    /** @var Adapter $db */
                    $db = $sm->get('Zend\Db\Adapter\Adapter');

                    return new PhotoTable(new TableGateway('photo', $db));

                },

                'VoteTable' => function (ServiceManager $sm) {

                    /** @var Adapter $db */
                    $db = $sm->get('Zend\Db\Adapter\Adapter');

                    return new VoteTable(new TableGateway('vote', $db));

                }

            )
        );
    }

    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'ShowPerPage' => function (AbstractPluginManager $pm) {

                    /** @var Request $request */
                    $request = $pm->getServiceLocator()->get('Request');

                    return new ShowPerPage($request);
                }
            )
        );
    }

}
