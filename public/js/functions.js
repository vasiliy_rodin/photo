$(function() {

    $('a[data-vote=true]').click(function(e) {
        var photoId = $(this).data('photo-id');
        var _this = this;
        $.post(
            $(this).data('url'),
            { photo_id: photoId },
            function (data) {
                if (data.success != true) {
                    alert('Вы уже голосовали');
                } else {
                    $(_this).parent().find('.votes').text(data.count);
                    alert('Голос засчитан');
                }
            }
        );
        return false;
    });

});