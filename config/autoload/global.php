<?php

return array(

    'social' => array(
        'facebook' => array(
            'class' => 'Social\Api\Facebook',
            'client_id'     => '744367408946658',
            'client_secret' => '336a59ed19b85a6c8ec6751d70eedc3d',
            'urls' => array(
                'auth_url'      => 'https://www.facebook.com/dialog/oauth',
                'token_url'     => 'https://graph.facebook.com/oauth/access_token',
                'redirect_url'  => '/social/auth/facebook/'
            )
        ),
    ),

    'db' => array(
        'adapters' => array(
            'Zend\Db\Adapter\Adapter' => array(
                'driver'         => 'Pdo',
                'dsn'            => 'mysql:dbname=pichesky;host=localhost',
                'driver_options' => array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
                ),
                'host' => 'localhost',
            ),
        )
    ),

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Db\Adapter\AdapterAbstractServiceFactory',
        ),
    ),

    'upload' => array(
        'extensions' => array(
            'image/jpeg' => array('jpeg', 'jpg'),
            'image/png'  => 'png',
        ),
        'max_size'        => 5 * 1000 * 1000,
        'destination_dir' => 'public/upload',
        'thumb'           => array(
            'width'       => 250,
            'height'      => 250,
        ),
    ),

    'domain' => 'pichesky.test.ru',
    'sender_mail' => 'test@test.ru',
    'sender_name' => 'test'

);
